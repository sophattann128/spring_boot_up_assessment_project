package com.up_project.up_first_spring_project;

import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@SecurityScheme(
        name = "bearerAuth",
        type = SecuritySchemeType.HTTP,
        scheme = "bearer"
)
public class UpFirstSpringProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(UpFirstSpringProjectApplication.class, args);
    }

}
