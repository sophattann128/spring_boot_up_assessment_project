package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.entities.Location;
import com.up_project.up_first_spring_project.model.request.LocationRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;

import java.util.List;

public interface LocationService {
    List<Location> getAllLocations(Pagination pagination);

    Location getLocationsById(long id);

    Location addNewLocation(LocationRequest locationRequest);

    Location updateLocation(long id, LocationRequest locationRequest);

    void deleteLocation(long id);
}
