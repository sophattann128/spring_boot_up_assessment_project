package com.up_project.up_first_spring_project.service.impl;
import com.up_project.up_first_spring_project.Repository.EmployeeRepository;
import com.up_project.up_first_spring_project.Repository.JobRepository;
import com.up_project.up_first_spring_project.Repository.LocationRepository;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Category;
import com.up_project.up_first_spring_project.model.entities.Employee;
import com.up_project.up_first_spring_project.model.entities.Job;
import com.up_project.up_first_spring_project.model.entities.Location;
import com.up_project.up_first_spring_project.model.projection.EmployeeProjection;
import com.up_project.up_first_spring_project.model.request.EmployeeRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
@RequiredArgsConstructor
public class EmployeeImpl implements EmployeeService{
    private final EmployeeRepository employeeRepository;
    private final JobRepository jobRepository;
    private final LocationRepository locationRepository;
    @Override
    public EmployeeProjection findEmployeeById(long id){
        return employeeRepository.findByEmployeeId(id);
    }
    @Override
    public List<EmployeeProjection> findAllEmployee(Pagination pagination){
        Page<EmployeeProjection> employees = employeeRepository.findEmployeeProjectionBy(PageRequest.of(pagination.getPage()-1, pagination.getSize()));
        pagination.setTotalCounts(employees.getTotalElements());
        return employees.getContent() ;
    }
    @Override
    public EmployeeProjection updateEmployeeById(long id, EmployeeRequest employeeRequest) {
        if (employeeRequest.getEmail().isBlank() ||
                employeeRequest.getFirstName().isBlank() ||
                employeeRequest.getLastName().isBlank() ||
                employeeRequest.getPhoneNumber().isBlank()
        ){
            throw new NullExceptionClass("Field NAME can not be empty", "Employee");
        }
        Employee foundEmployee = employeeRepository.findById(id).orElse(null);
        Job job = jobRepository.findById(id).orElse(null);
        Location location = locationRepository.findById(id).orElse(null);
        if(job == null){
            throw new NotFoundExceptionClass("This job id " + employeeRequest.getJobId() + " doesn't exist!", "Job");
        }
        if(location == null){
            throw new NotFoundExceptionClass("This location id " + employeeRequest.getLocationId() + " doesn't exist!", "Location");

        }
        if(foundEmployee == null){
            System.out.println("Employee not found");
        }
        foundEmployee.setFirstName(employeeRequest.getFirstName().trim());
        foundEmployee.setLastName(employeeRequest.getLastName().trim());
        foundEmployee.setPhoneNumber(employeeRequest.getPhoneNumber().trim());
        foundEmployee.setEmail(employeeRequest.getEmail().trim());
        foundEmployee.setJob(job);
        foundEmployee.setLocation(location);
        employeeRepository.save(foundEmployee);
        return  employeeRepository.findByEmployeeId(id);
    }
    @Override
    public String deleteEmployeeById(long id) {
        employeeRepository.deleteById(id);
        return "Successfully deleted!";
    }
    @Override
    public EmployeeProjection addEmployee(EmployeeRequest employeeRequest){
        if (employeeRequest.getEmail().isBlank() ||
                employeeRequest.getFirstName().isBlank() ||
                employeeRequest.getLastName().isBlank() ||
                employeeRequest.getPhoneNumber().isBlank()
        ){
            throw new NullExceptionClass("Field NAME can not be empty", "Employee");
        }
        Job job = jobRepository.findById(employeeRequest.getJobId()).orElse(null);
        Location location = locationRepository.findById(employeeRequest.getLocationId()).orElse(null);
        if(job == null){
            throw new NotFoundExceptionClass("This job id " + employeeRequest.getJobId() + " doesn't exist!", "Job");
        }
        if(location == null){
            throw new NotFoundExceptionClass("This location id " + employeeRequest.getLocationId() + " doesn't exist!", "Location");

        }
        Employee employee = Employee.builder()
                .firstName(employeeRequest.getFirstName().trim())
                .lastName(employeeRequest.getLastName().trim())
                .phoneNumber(employeeRequest.getPhoneNumber().trim())
                .job(job)
                .location(location)
                .build();

        employeeRepository.save(employee);
        return findEmployeeById(employee.getEmployeeId());
    }
}
