package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.ProductRepository;
import com.up_project.up_first_spring_project.Repository.ShopRepository;
import com.up_project.up_first_spring_project.Repository.StockRepository;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Product;
import com.up_project.up_first_spring_project.model.entities.Shop;
import com.up_project.up_first_spring_project.model.entities.Stock;
import com.up_project.up_first_spring_project.model.projection.SaleProjection;
import com.up_project.up_first_spring_project.model.projection.StockProjection;
import com.up_project.up_first_spring_project.model.request.StockRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.StockService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StockServiceImpl implements StockService {
    private final StockRepository stockRepository;
    private final ProductRepository productRepository;
    private final ShopRepository shopRepository;

    @Override
    public List<StockProjection> getAllStock(Pagination pagination) {
        Page<StockProjection> stocks = stockRepository.findStockProjectionBy(PageRequest.of(pagination.getPage()-1, pagination.getSize()));
        pagination.setTotalCounts(stocks.getTotalElements());
        return stocks.getContent() ;

    }

    @Override
    public StockProjection getStockById(long id) {
        StockProjection stock = stockRepository.findStockProjectionByStockId(id);
        if (stock == null) {
            throw new NotFoundExceptionClass("This stock id " + id + " doesn't exist!", "Stock");
        }

        return stock;
    }

    @Override
    public StockProjection addNewStock(StockRequest stockRequest) {
        Product product = productRepository.findById(stockRequest.getProductId()).orElse(null);
        Shop shop = shopRepository.findById(stockRequest.getShopId()).orElse(null);
        if (product == null || shop == null) {
            throw new NotFoundExceptionClass("Product or shop id doesn't exist!", "Stock");
        }

        Stock stock = Stock.builder()
                .product(product)
                .productPrice(stockRequest.getProductPrice())
                .productAmount(stockRequest.getProductAmount())
                .shop(shop)
                .build();
        stockRepository.save(stock);
        return stockRepository.findStockProjectionByStockId(stock.getStockId());
    }

    @Override
    public StockProjection updateStock(long id, StockRequest stockRequest) {
        Product product = productRepository.findById(stockRequest.getProductId()).orElse(null);
        Shop shop = shopRepository.findById(stockRequest.getShopId()).orElse(null);
        Stock stockToUpdate = stockRepository.findById(id).orElse(null);
        if (product == null) {
            throw new NotFoundExceptionClass("This product id " + stockRequest.getProductId() + " doesn't exist!", "Stock");
        }
        if (shop == null) {
            throw new NotFoundExceptionClass("This shop id " + stockRequest.getShopId() + " doesn't exist!", "Stock");
        }
        if (stockToUpdate == null) {
            throw new NotFoundExceptionClass("This stock id " + id + " doesn't exist!", "Stock");
        }
        stockToUpdate.setProductPrice(stockRequest.getProductPrice());
        stockToUpdate.setProductAmount(stockRequest.getProductAmount());
        stockToUpdate.setProduct(product);
        stockToUpdate.setShop(shop);
        stockRepository.save(stockToUpdate);

        return getStockById(id);
    }

    @Override
    public void deleteStockById(long id) {
        Stock foundStock = stockRepository.findById(id).orElse(null);
        if (foundStock == null) {
            throw new NotFoundExceptionClass("This stock id " + id +" doesn't exist!", "category");
        }
        stockRepository.deleteById(id);
    }
}
