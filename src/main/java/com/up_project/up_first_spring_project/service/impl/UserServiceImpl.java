package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.EmployeeRepository;
import com.up_project.up_first_spring_project.Repository.RoleRepository;
import com.up_project.up_first_spring_project.Repository.UserRepository;
import com.up_project.up_first_spring_project.exception.ForbiddenException;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Employee;
import com.up_project.up_first_spring_project.model.entities.Role;
import com.up_project.up_first_spring_project.model.entities.User;
import com.up_project.up_first_spring_project.model.projection.UserProjection;
import com.up_project.up_first_spring_project.model.request.UserRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService, UserDetailsService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmployeeRepository employeeRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User myUser = userRepository.findUserByUsername(username);
        if (myUser == null) {
            throw new UsernameNotFoundException("User Not Found");
        }
        return myUser;
    }


    public String getUserName(String loginUsername) {
        String username = userRepository.findUserByUsername(loginUsername).getUsername();
        if (username == null) {
            throw new UsernameNotFoundException("User Not Found");
        }
        return username;
    }

    @Override
    public long getUserId(String usernameOfCurrentUser) {

        return userRepository.findUserByUsername(usernameOfCurrentUser).getUserId();
    }


    public long currentUserId(Authentication authentication) {
        User user = userRepository.findUserByUsername(authentication.getName());
        return user.getUserId();
    }


    @Override
    public UserProjection findUserById(long id) {
        return userRepository.findByUserId(id);
    }

    @Override
    public List<UserProjection> findAllUser(Pagination pagination) {
        Page<UserProjection> users = userRepository.findUserProjectionBy(PageRequest.of(pagination.getPage() - 1, pagination.getSize()));
        pagination.setTotalCounts(users.getTotalElements());
        return users.getContent();

    }

    @Override
    public UserProjection updateUserById(long id, UserRequest userRequest) {
        if (userRequest.getPassword().isBlank() ||
                userRequest.getName().isBlank()
        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "User");
        }
        Role role = roleRepository.findById(userRequest.getRoleId()).orElse(null);
        if (role == null) {
            throw new NotFoundExceptionClass("This saleDetail id " + userRequest.getRoleId() + " doesn't exist!", "Role");
        }
        Employee employee = employeeRepository.findById(userRequest.getEmployeeId()).orElse(null);
        if (employee == null) {
            throw new NotFoundExceptionClass("This saleDetail id " + userRequest.getRoleId() + " doesn't exist!", "Employee");

        }
        User foundUser = userRepository.findById(id).orElse(null);

        if (foundUser == null) {
            throw new NotFoundExceptionClass("This saleDetail id " + userRequest.getRoleId() + " doesn't exist!", "Employee");

        }
        foundUser.setUsername(userRequest.getName());
        foundUser.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        foundUser.setRole(role);
        foundUser.setEmployee(employee);
        userRepository.save(foundUser);

        return userRepository.findByUserId(id);
    }

    @Override
    public void deleteUserById(long id) {
        long userId = currentUserId(SecurityContextHolder.getContext().getAuthentication());
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            throw new NotFoundExceptionClass("This user id " + id + " doesn't exist!", "User");
        }
        if (id == userId) {
            throw new ForbiddenException("This user id " + id + " can't be deleted because this user is currently login", "User");
        }
        userRepository.deleteById(id);
    }

    @Override
    public User addUser(UserRequest userRequest) {
        if (userRequest.getPassword().isBlank() ||
                userRequest.getName().isBlank()
        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Supplier");
        }
        Role role = roleRepository.findById(userRequest.getRoleId()).orElse(null);
        if (role == null) {
            throw new NotFoundExceptionClass("This role id " + userRequest.getRoleId() + " doesn't exist!", "Role");
        }
        Employee employee = employeeRepository.findById(userRequest.getEmployeeId()).orElse(null);
        if (employee == null) {
            throw new NotFoundExceptionClass("This employee id " + userRequest.getEmployeeId() + " doesn't exist!", "Employee");

        }
        User foundUser = userRepository.findUserByUsername(userRequest.getName());
        if (foundUser == null) {
            User user = User.builder()
                    .username(userRequest.getName())
                    .password(passwordEncoder.encode(userRequest.getPassword()))
                    .role(role)
                    .employee(employee)
                    .build();
            return userRepository.save(user);

        }
        return null;

    }
}
