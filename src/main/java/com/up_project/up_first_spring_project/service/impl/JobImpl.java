package com.up_project.up_first_spring_project.service.impl;
import com.up_project.up_first_spring_project.Repository.JobRepository;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Category;
import com.up_project.up_first_spring_project.model.entities.Job;
import com.up_project.up_first_spring_project.model.projection.JobProjection;
import com.up_project.up_first_spring_project.model.request.JobRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.JobService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class JobImpl implements JobService {
    private final JobRepository jobRepository;

    @Override
    public JobProjection findJobById(long id) {
        return jobRepository.findByJobId(id);
    }

    @Override
    public List<JobProjection> findAllJob(Pagination pagination){
        Page<JobProjection> jobs = jobRepository.findJobProjectionBy(PageRequest.of(pagination.getPage()-1, pagination.getSize()));
        pagination.setTotalCounts(jobs.getTotalElements());
        return jobs.getContent() ;

    }

    @Override
    public JobProjection updateJobById(long id, JobRequest jobRequest) {
        if (jobRequest.getJobTitle().isBlank() ||
                Double.toString(jobRequest.getSalary()).isBlank()
        ){
            throw new NullExceptionClass("Field NAME can not be empty", "Job");
        }
        Job foundJob = jobRepository.findById(id).orElse(null);
        if(foundJob == null){
            System.out.println("Job not found");
        }
        foundJob.setJobId(jobRequest.getJobId());
        foundJob.setJobTitle(jobRequest.getJobTitle().trim());
        foundJob.setSalary(jobRequest.getSalary());
        foundJob.setUpdatedDate(LocalDateTime.now());
        jobRepository.save(foundJob);
        return  jobRepository.findByJobId(id);
    }

    @Override
    public void deleteJobById(long id) {
         jobRepository.deleteById(id);
    }

    @Override
    public Job addJob(JobRequest jobRequest){
        if (jobRequest.getJobTitle().isBlank() ||
                Double.toString(jobRequest.getSalary()).isBlank()
        ){
            throw new NullExceptionClass("Field NAME can not be empty", "Job");
        }
        Job job = Job.builder()
                .jobTitle(jobRequest.getJobTitle().trim())
                .salary(jobRequest.getSalary())
                .build();

        return jobRepository.save(job);
    }
}