package com.up_project.up_first_spring_project.service.impl;
import com.up_project.up_first_spring_project.Repository.RoleRepository;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Job;
import com.up_project.up_first_spring_project.model.entities.Role;
import com.up_project.up_first_spring_project.model.projection.RoleProjection;
import com.up_project.up_first_spring_project.model.request.RoleRequest;
import com.up_project.up_first_spring_project.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleImpl implements RoleService {
    private final RoleRepository roleRepository;
    @Override
    public RoleProjection findRoleById(long id){
        return roleRepository.findByRoleId(id);
    }

    @Override
    public List<RoleProjection> findAllRole(){
        return roleRepository.findRoleProjectionBy();
    }

    @Override
    public RoleProjection updateRoleById(long id, RoleRequest roleRequest){
        if (roleRequest.getRole().isBlank()

        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Role");
        }
        Role foundRole = roleRepository.findById(id).orElse(null);
        if(foundRole == null){
            throw new NotFoundExceptionClass("This role id " + id + " doesn't exist!", "Role");

        }
        foundRole.setRoleId(roleRequest.getRoleId());
        foundRole.setRole(roleRequest.getRole().trim());

        roleRepository.save(foundRole);
        return  roleRepository.findByRoleId(id);
    }

    @Override
    public void deleteRoleById(long id){
        roleRepository.deleteById(id);
    }

    @Override
    public Role addRole(RoleRequest roleRequest){
        if (roleRequest.getRole().isBlank()

        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Role");
        }

        Role role = Role.builder()
                .role(roleRequest.getRole().trim())
                .build();

        return roleRepository.save(role);
    }
}
