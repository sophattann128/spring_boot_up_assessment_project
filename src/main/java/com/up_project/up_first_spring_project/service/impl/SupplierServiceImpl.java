package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.LocationRepository;
import com.up_project.up_first_spring_project.Repository.SupplierRepository;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Location;
import com.up_project.up_first_spring_project.model.entities.Supplier;
import com.up_project.up_first_spring_project.model.projection.SupplierProjection;
import com.up_project.up_first_spring_project.model.request.SupplierRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SupplierServiceImpl implements SupplierService {
    private final SupplierRepository supplierRepository;
    private final LocationRepository locationRepository;

    @Override
    public List<Supplier> getAllSuppliers(Pagination pagination) {
        Page<Supplier> suppliers = supplierRepository.findAll(PageRequest.of(pagination.getPage()-1, pagination.getSize()));
        pagination.setTotalCounts(suppliers.getTotalElements());
        return suppliers.getContent() ;
    }

    @Override
    public Supplier getSupplierById(long id) {
        Supplier supplier = supplierRepository.findById(id).orElse(null);
        if(supplier == null){
            throw new NotFoundExceptionClass("This supplier id " + id + " doesn't exist!", "Supplier");
        }
        return supplier;
    }

    @Override
    public Supplier addNewSupplier(SupplierRequest supplierRequest) {
        if (supplierRequest.getCompanyName().isBlank() ||
                supplierRequest.getPhoneNumber().isBlank()
        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Supplier");
        }
        Location location = locationRepository.findById(supplierRequest.getLocationId()).orElse(null);
        if(location == null){
            throw new NotFoundExceptionClass("This location id "  +supplierRequest.getLocationId() + " doesn't exist!", "Location");
        }
        Supplier supplier = Supplier.builder()
                .companyName(supplierRequest.getCompanyName().trim())
                .phoneNumber(supplierRequest.getPhoneNumber().trim())
                .location(location)
                .build();
        return supplierRepository.save(supplier);
    }

    @Override
    public Supplier updateSupplier(long id, SupplierRequest supplierRequest) {
        if (supplierRequest.getCompanyName().isBlank() ||
                supplierRequest.getPhoneNumber().isBlank()
        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Supplier");
        }
        Location location = locationRepository.findById(supplierRequest.getLocationId()).orElse(null);
        if(location == null){
            throw new NotFoundExceptionClass("This location id "  +supplierRequest.getLocationId() + " doesn't exist!", "Location");
        }
        Supplier supplierToUpdate = getSupplierById(id);
        supplierToUpdate.setCompanyName(supplierRequest.getCompanyName().trim());
        supplierToUpdate.setPhoneNumber(supplierRequest.getPhoneNumber().trim());
        supplierToUpdate.setLocation(location);
        return supplierRepository.save(supplierToUpdate);
    }

    @Override
    public void deleteSupplier(long id) {
        getSupplierById(id);
        supplierRepository.deleteById(id);
    }

}
