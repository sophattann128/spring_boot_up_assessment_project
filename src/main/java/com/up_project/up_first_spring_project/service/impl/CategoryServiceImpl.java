package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.CategoryRepository;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Category;
import com.up_project.up_first_spring_project.model.request.CategoryRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

//@Service

//@Component
@Service

@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;
    @Override
    public List<Category> getAllCategories(Pagination pagination) {
        Page<Category> categories = categoryRepository.findAll(PageRequest.of(pagination.getPage()-1, pagination.getSize()));
        pagination.setTotalCounts(categories.getTotalElements());
        return categories.getContent() ;

    }

    @Override
    public Category getCategoryById(Long id) {
        return categoryRepository.findById(id).get();
    }

    @Override
    public Category addCategory(CategoryRequest categoryRequest) {
        if (categoryRequest.getCategoryName().isBlank() ||
                categoryRequest.getCategoryDescription().isBlank()){
            throw new NullExceptionClass("Field NAME can not be empty", "Category");
        }
        Category category = Category.builder()
                .categoryName(categoryRequest.getCategoryName().trim())
                .categoryDescription(categoryRequest.getCategoryDescription().trim())
                .build();
        return categoryRepository.save(category);
    }
    @Override
    public Category update(Long id, CategoryRequest categoryRequest) {
        if (categoryRequest.getCategoryName().isBlank() ||
                categoryRequest.getCategoryDescription().isBlank()){
            throw new NullExceptionClass("Field NAME can not be empty", "Category");
        }
        Category foundCategory = getCategoryById(id);
        if(foundCategory == null){
            throw new NotFoundException("This id is not found!");
        }
        foundCategory.setCategoryName(categoryRequest.getCategoryName().trim());
        foundCategory.setCategoryDescription(categoryRequest.getCategoryDescription().trim());
        return categoryRepository.save(foundCategory);
    }

    @Override
    public void deleteById(Long id) {

        categoryRepository.deleteById(id);
    }
}
