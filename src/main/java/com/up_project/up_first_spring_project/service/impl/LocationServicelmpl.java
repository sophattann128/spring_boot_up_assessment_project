package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.LocationRepository;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Location;
import com.up_project.up_first_spring_project.model.request.LocationRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.LocationService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class LocationServicelmpl implements LocationService {
    private final LocationRepository locationRepository;
    @Override
    public List<Location> getAllLocations(Pagination pagination) {
        Page<Location> locations = locationRepository.findAll(PageRequest.of(pagination.getPage()-1, pagination.getSize()));
        pagination.setTotalCounts(locations.getTotalElements());
        return locations.getContent() ;
    }

    @Override
    public Location getLocationsById(long id) {
        Location location = locationRepository.findById(id).orElse(null);
        if(location == null){
            throw new NotFoundExceptionClass("This location id " + id + " doesn't exist!", "Location");

        }

        return location;

    }

    @Override
    public Location addNewLocation(LocationRequest locationRequest) {
        if (locationRequest.getCity().isBlank() ||
                locationRequest.getProvince().isBlank() ||
                locationRequest.getStreet().isBlank()
        ){
            throw new NullExceptionClass("Field NAME can not be empty", "Location");
        }
        Location location = Location.builder()
                .city(locationRequest.getCity().trim())
                .province(locationRequest.getProvince().trim())
                .street(locationRequest.getStreet().trim())
                .build();
        return locationRepository.save(location);
    }

    @Override
    public Location updateLocation(long id, LocationRequest locationRequest) {
        if (locationRequest.getCity().isBlank() ||
                locationRequest.getProvince().isBlank() ||
                locationRequest.getStreet().isBlank()
        ){
            throw new NullExceptionClass("Field NAME can not be empty", "Location");
        }
        Location foundLocation = getLocationsById(id);
        foundLocation.setCity(locationRequest.getCity());
        foundLocation.setProvince(locationRequest.getProvince());
        foundLocation.setStreet(locationRequest.getStreet());
        return locationRepository.save(foundLocation);
    }

    @Override
    public void deleteLocation(long id) {
        getLocationsById(id);
        locationRepository.deleteById(id);

    }
}
