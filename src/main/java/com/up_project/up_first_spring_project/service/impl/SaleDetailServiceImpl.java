package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.ProductRepository;
import com.up_project.up_first_spring_project.Repository.SaleDetailRepository;
import com.up_project.up_first_spring_project.Repository.SaleRepository;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.*;
import com.up_project.up_first_spring_project.model.projection.ProductProjection;
import com.up_project.up_first_spring_project.model.projection.SaleDetailProjection;
import com.up_project.up_first_spring_project.model.request.SaleDetailRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.ProductService;
import com.up_project.up_first_spring_project.service.SaleDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SaleDetailServiceImpl implements SaleDetailService {
    private final SaleDetailRepository saleDetailRepository;
    private final ProductRepository productRepository;
    private final SaleRepository saleRepository;

    @Override
    public List<SaleDetailProjection> getAllSaleDetail(Pagination pagination) {
        Page<SaleDetailProjection> saleDetails = saleDetailRepository.findSaleDetailProjectionBy(PageRequest.of(pagination.getPage() - 1, pagination.getSize()));
        pagination.setTotalCounts(saleDetails.getTotalElements());
        return saleDetails.getContent();
    }

    @Override
    public SaleDetailProjection getSaleDetailById(long id) {
        SaleDetail saleDetail = saleDetailRepository.findById(id).orElse(null);
        if (saleDetail == null) {
            throw new NotFoundExceptionClass("This saleDetail id " + id + " doesn't exist!", "SaleDetail");
        }
        return saleDetailRepository.findSaleDetailBySaleDetailId(id);
    }

    @Override
    public SaleDetailProjection addSaleDetail(SaleDetailRequest saleDetailRequest) {

        Sale sale = saleRepository.findById(saleDetailRequest.getSaleId()).orElse(null);
        Product product = productRepository.findById(saleDetailRequest.getProductId()).orElse(null);
        if (sale == null) {
            throw new NotFoundExceptionClass("This sale id " + saleDetailRequest.getSaleId() + " doesn't exist!", "SaleDetail");
        }
        if (product == null) {
            throw new NotFoundExceptionClass("This product id " + saleDetailRequest.getProductId() + " doesn't exist!", "SaleDetail");

        }
        SaleDetail saleDetail = SaleDetail.builder()
                .amount(saleDetailRequest.getAmount())
                .product(product)
                .sale(sale)
                .build();
        saleDetailRepository.save(saleDetail);
        return getSaleDetailById(saleDetail.getSaleDetailId());
    }

    @Override
    public SaleDetailProjection updateSaleDetail(long id, SaleDetailRequest saleDetailRequest) {
        Product product = productRepository.findById(saleDetailRequest.getProductId()).orElse(null);
        Sale sale = saleRepository.findById(saleDetailRequest.getSaleId()).orElse(null);
        SaleDetail saleDetailToUpdate = saleDetailRepository.findById(id).orElse(null);
        if (product == null) {
            throw new NotFoundExceptionClass("This product id " + saleDetailRequest.getProductId() + " doesn't exist!", "Stock");
        }
        if (sale == null) {
            throw new NotFoundExceptionClass("This sale id " + saleDetailRequest.getSaleId() + " doesn't exist!", "Stock");
        }
        if (saleDetailToUpdate == null) {
            throw new NotFoundExceptionClass("This saleDetail id " + id + " doesn't exist!", "Stock");
        }
        saleDetailToUpdate.setAmount(saleDetailRequest.getAmount());
        saleDetailToUpdate.setProduct(product);
        saleDetailToUpdate.setSale(sale);
        saleDetailRepository.save(saleDetailToUpdate);

        return getSaleDetailById(id);
    }

    @Override
    public void deleteSaleDetail(long id) {
        SaleDetail foundSaleDetail = saleDetailRepository.findById(id).orElse(null);
        if (foundSaleDetail == null) {
            throw new NotFoundExceptionClass("This saleDetail id " + id + " doesn't exist!", "category");
        }
        saleDetailRepository.deleteById(id);
    }

}
