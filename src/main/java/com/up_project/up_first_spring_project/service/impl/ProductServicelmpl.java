package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.CategoryRepository;
import com.up_project.up_first_spring_project.Repository.ProductRepository;
import com.up_project.up_first_spring_project.Repository.SupplierRepository;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Category;
import com.up_project.up_first_spring_project.model.entities.Product;
import com.up_project.up_first_spring_project.model.entities.Supplier;
import com.up_project.up_first_spring_project.model.projection.JobProjection;
import com.up_project.up_first_spring_project.model.projection.ProductProjection;
import com.up_project.up_first_spring_project.model.request.ProductRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.CategoryService;
import com.up_project.up_first_spring_project.service.ProductService;
import com.up_project.up_first_spring_project.service.SupplierService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServicelmpl implements ProductService {
    private final ProductRepository productRepository;
    private final CategoryService categoryService;
    private final SupplierService supplierService;

    @Override
    public List<ProductProjection> getAllProduct(Pagination pagination) {
        Page<ProductProjection> products = productRepository.findProductProjectionBy(PageRequest.of(pagination.getPage() - 1, pagination.getSize()));
        pagination.setTotalCounts(products.getTotalElements());
        return products.getContent();
    }

    @Override
    public ProductProjection getProductById(long id) {
        return productRepository.findByProductId(id);
    }



    @Override
    public ProductProjection updateById(long id, ProductRequest productRequest) {
        if (productRequest.getProductName().isBlank()

        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Product");
        }
        Product product = productRepository.findById(id).orElse(null);
        Category category = categoryService.getCategoryById(productRequest.getCategoryId());
        Supplier supplier = supplierService.getSupplierById(productRequest.getSupplierId());
        product.setProductName(productRequest.getProductName().trim());
        product.setProductPrice(productRequest.getProductPrice());
        product.setCategory(category);
        product.setSupplier(supplier);
        productRepository.save(product);
        return productRepository.findByProductId(id);
    }

    @Override
    public String deleteProductById(long id) {
        productRepository.deleteById(id);
        return "Successfully deleted!";
    }

    @Override
    public ProductProjection addNewProduct(ProductRequest productRequest) {
        if (productRequest.getProductName().isBlank()

        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Product");
        }
        Category category = categoryService.getCategoryById(productRequest.getCategoryId());
        Supplier supplier = supplierService.getSupplierById(productRequest.getSupplierId());

        Product product = Product.builder()
                .productName(productRequest.getProductName())
                .productPrice(productRequest.getProductPrice())
                .category(category)
                .supplier(supplier)
                .build();
        productRepository.save(product);
        return getProductById(product.getProductId());
    }


}
