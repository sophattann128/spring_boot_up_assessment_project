package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.*;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.*;
import com.up_project.up_first_spring_project.model.projection.ProductProjection;
import com.up_project.up_first_spring_project.model.projection.SaleProjection;
import com.up_project.up_first_spring_project.model.request.SaleRequest;
import com.up_project.up_first_spring_project.model.request.Sale_and_SaleDetailRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.SaleService;
import com.up_project.up_first_spring_project.service.ShopService;
import com.up_project.up_first_spring_project.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class SaleServicelmpl implements SaleService {
    private final SaleRepository saleRepository;
    private final UserService userService;
    private final ShopRepository shopRepository;
    private final UserRepository userRepository;
    private final SaleDetailRepository saleDetailRepository;
    private final ProductRepository productRepository;

    @Override
    public List<SaleProjection> getAllSale(Pagination pagination) {
        Page<SaleProjection> sales = saleRepository.findSaleProjectionBy(PageRequest.of(pagination.getPage() - 1, pagination.getSize()));
        pagination.setTotalCounts(sales.getTotalElements());
        return sales.getContent();

    }

    @Override
    public SaleProjection getSaleById(long id) {
        return saleRepository.findBySaleId(id);
    }

    @Override
    public List<SaleProjection> getAllSaleFromCurrentUser(Pagination pagination) {
        long userId = userService.currentUserId(SecurityContextHolder.getContext().getAuthentication());
        System.out.println("userId " + userId);
        return saleRepository.getAllSaleFromCurrentUser(userId);
    }

    @Override
    public SaleProjection addNewSale(Sale_and_SaleDetailRequest saleRequest) {
        long userId = userService.currentUserId(SecurityContextHolder.getContext().getAuthentication());
        Shop shop = shopRepository.findById(saleRequest.getShopId()).orElse(null);
        Product product = productRepository.findById(saleRequest.getProductId()).orElse(null);

        User user = userRepository.findById(userId).orElse(null);

        Sale sale = Sale.builder()
                .user(user)
                .shop(shop)
                .date(LocalDateTime.now())
                .build();
        SaleDetail saleDetail = SaleDetail.builder()
                .sale(sale)
                .amount(saleRequest.getAmount())
                .product(product)
                .build();
        saleRepository.save(sale);
        saleDetailRepository.save(saleDetail);

        return getSaleById(sale.getSaleId());
    }

    @Override
    public SaleProjection updateSale(long id, SaleRequest saleRequest) {
        long userId = userService.currentUserId(SecurityContextHolder.getContext().getAuthentication());

        Shop shop = shopRepository.findById(saleRequest.getShopId()).orElse(null);


        User user = userRepository.findById(userId).orElse(null);

        Sale sale = saleRepository.findById(id).orElse(null);
        if (sale == null) {
            throw new NotFoundExceptionClass("This sale id " + id + " doesn't exist!", "Sale");
        }
        sale.setUser(user);
        sale.setShop(shop);
        sale.setDate(LocalDateTime.now());
        saleRepository.save(sale);
        return getSaleById(id);
    }

    @Override
    public void deleteSale(long id) {
        SaleProjection sale = saleRepository.findBySaleId(id);
        if (sale == null) {
            throw new NotFoundExceptionClass("This sale id " + id + " doesn't exist!", "Sale");

        }
        saleRepository.deleteById(id);
    }
}
