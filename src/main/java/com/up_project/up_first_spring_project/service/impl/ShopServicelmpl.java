package com.up_project.up_first_spring_project.service.impl;

import com.up_project.up_first_spring_project.Repository.ShopRepository;
import com.up_project.up_first_spring_project.Repository.StockRepository;
import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.exception.NullExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Shop;
import com.up_project.up_first_spring_project.model.entities.Stock;
import com.up_project.up_first_spring_project.model.projection.ShopProjection;
import com.up_project.up_first_spring_project.model.request.ShopRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.service.ShopService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class ShopServicelmpl  implements ShopService {
    private final ShopRepository shopRepository;
    private final StockRepository stockRepository;
    @Override
    public List<Shop> getAllShops(Pagination pagination) {
        Page<Shop> shops = shopRepository.findAll(PageRequest.of(pagination.getPage()-1, pagination.getSize()));
        pagination.setTotalCounts(shops.getTotalElements());
        return shops.getContent() ;

    }

    @Override
    public ShopProjection getAllShopsById(long id) {
        ShopProjection shop = shopRepository.findByShopId(id);
        if(shop == null){
            throw new NotFoundExceptionClass("This shop id " + id + " doesn't exist!", "Shop");

        }
        return shop;
    }

    @Override
    public Shop addNewShop(ShopRequest shopRequest) {
        if (shopRequest.getAddress().isBlank() ||
                shopRequest.getBranchName().isBlank()

        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Shop");
        }
        Shop shop = Shop.builder()
                .address(shopRequest.getAddress().trim())
                .branchName(shopRequest.getBranchName().trim())
                .build();
        return shopRepository.save(shop);
    }

    @Override
    public Shop updateShop(long id, ShopRequest shopRequest) {
        if (shopRequest.getAddress().isBlank() ||
                shopRequest.getBranchName().isBlank()

        ) {
            throw new NullExceptionClass("Field NAME can not be empty", "Shop");
        }
        Shop shop = shopRepository.findById(id).orElse(null);
        shop.setAddress(shop.getAddress());
        shop.setBranchName(shop.getBranchName());
        return shopRepository.save(shop);
    }

    @Override
    public void deleteShop(long id) {
        Shop shop = shopRepository.findById(id).orElse(null);
        if(shop == null){
            throw new NotFoundExceptionClass("This shop id " + id + " doesn't exist!", "Shop");

        }
//        List<Stock> stocks = shop.getStocks();
//        stockRepository.deleteAll(stocks);
        shopRepository.deleteById(id);
    }
}
