package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.entities.Stock;
import com.up_project.up_first_spring_project.model.projection.StockProjection;
import com.up_project.up_first_spring_project.model.request.StockRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;

import java.util.List;

public interface StockService {
    List<StockProjection> getAllStock(Pagination pagination);

    StockProjection getStockById(long id);

    StockProjection addNewStock(StockRequest stockRequest);

    StockProjection updateStock(long id, StockRequest stockRequest);

    void deleteStockById(long id);
}
