package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.entities.SaleDetail;
import com.up_project.up_first_spring_project.model.projection.SaleDetailProjection;
import com.up_project.up_first_spring_project.model.request.SaleDetailRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;

import java.util.List;

public interface SaleDetailService {
    List<SaleDetailProjection> getAllSaleDetail(Pagination pagination);

    SaleDetailProjection getSaleDetailById(long id);

    SaleDetailProjection addSaleDetail(SaleDetailRequest saleDetailRequest);

    SaleDetailProjection updateSaleDetail(long id, SaleDetailRequest saleDetailRequest);

    void deleteSaleDetail(long id);
}
