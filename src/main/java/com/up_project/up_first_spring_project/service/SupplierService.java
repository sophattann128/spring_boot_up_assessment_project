package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.entities.Supplier;
import com.up_project.up_first_spring_project.model.request.SupplierRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;

import java.util.List;

public interface SupplierService {
    List<Supplier> getAllSuppliers(Pagination pagination);

    Supplier getSupplierById(long id);

    Supplier addNewSupplier(SupplierRequest supplierRequest);

    Supplier updateSupplier(long id, SupplierRequest supplierRequest);

    void deleteSupplier(long id);
}
