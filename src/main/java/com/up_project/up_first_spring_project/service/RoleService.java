package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.entities.Role;
import com.up_project.up_first_spring_project.model.projection.RoleProjection;
import com.up_project.up_first_spring_project.model.request.RoleRequest;

import java.util.List;

public interface RoleService {
    RoleProjection findRoleById(long id);
    List<RoleProjection> findAllRole();
    RoleProjection updateRoleById(long id, RoleRequest roleRequest);
    void deleteRoleById(long id);
    Role addRole(RoleRequest roleRequest);

}
