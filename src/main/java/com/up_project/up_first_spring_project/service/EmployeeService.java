package com.up_project.up_first_spring_project.service;
import com.up_project.up_first_spring_project.model.entities.Employee;
import com.up_project.up_first_spring_project.model.projection.EmployeeProjection;
import com.up_project.up_first_spring_project.model.request.EmployeeRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;

import java.util.List;

public interface EmployeeService {
    EmployeeProjection findEmployeeById(long id);
    List<EmployeeProjection> findAllEmployee(Pagination pagination);

    EmployeeProjection updateEmployeeById(long id, EmployeeRequest employeeRequest);

    String deleteEmployeeById(long id);
    EmployeeProjection addEmployee(EmployeeRequest employeeRequest);
}
