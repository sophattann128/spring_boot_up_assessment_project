package com.up_project.up_first_spring_project.service;
import com.up_project.up_first_spring_project.model.entities.Job;
import com.up_project.up_first_spring_project.model.projection.JobProjection;
import com.up_project.up_first_spring_project.model.request.JobRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import org.springframework.data.domain.Page;

import java.util.List;

public interface JobService {
    JobProjection findJobById(long id);
    List<JobProjection> findAllJob(Pagination pagination);
    JobProjection updateJobById(long id, JobRequest jobRequest);
    void deleteJobById(long id);
    Job addJob(JobRequest jobRequest);
}
