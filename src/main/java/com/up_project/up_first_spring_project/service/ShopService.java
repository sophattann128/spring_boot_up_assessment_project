package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.entities.Shop;
import com.up_project.up_first_spring_project.model.projection.ShopProjection;
import com.up_project.up_first_spring_project.model.request.ShopRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;

import java.util.List;

public interface ShopService {
    List<Shop> getAllShops(Pagination pagination);

    ShopProjection getAllShopsById(long id);

    Shop addNewShop(ShopRequest shopRequest);

    Shop updateShop(long id, ShopRequest shopRequest);

    void deleteShop(long id);
}
