package com.up_project.up_first_spring_project.service;
import com.up_project.up_first_spring_project.model.entities.User;
import com.up_project.up_first_spring_project.model.projection.UserProjection;
import com.up_project.up_first_spring_project.model.request.UserRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
public interface UserService extends UserDetailsService {
    UserProjection findUserById(long id);
    List<UserProjection> findAllUser(Pagination pagination);
    UserProjection updateUserById(long id, UserRequest userRequest);
    void deleteUserById(long id);
    User addUser(UserRequest userRequest);

    UserDetails loadUserByUsername(String email);

    long currentUserId(Authentication authentication);

    String getUserName(String loginUsername);
    long getUserId(String usernameOfCurrentUser);

}
