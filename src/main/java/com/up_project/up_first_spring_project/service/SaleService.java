package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.projection.SaleProjection;
import com.up_project.up_first_spring_project.model.request.SaleRequest;
import com.up_project.up_first_spring_project.model.request.Sale_and_SaleDetailRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import org.springframework.security.core.Authentication;

import java.util.List;

public interface SaleService {
    List<SaleProjection> getAllSale(Pagination pagination);
    SaleProjection getSaleById(long id);

    List<SaleProjection> getAllSaleFromCurrentUser(Pagination pagination);

    SaleProjection addNewSale(Sale_and_SaleDetailRequest saleRequest);

    SaleProjection updateSale(long id, SaleRequest saleRequest);

    void deleteSale(long id);
}

