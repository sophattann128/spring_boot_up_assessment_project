package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.entities.Category;
import com.up_project.up_first_spring_project.model.request.CategoryRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface CategoryService {
    List<Category> getAllCategories(Pagination pagination);

    Category getCategoryById(Long id);

    Category addCategory(CategoryRequest categoryRequest);

    Category update(Long id, CategoryRequest categoryRequest);

    void deleteById(Long id);
}
