package com.up_project.up_first_spring_project.service;

import com.up_project.up_first_spring_project.model.projection.ProductProjection;
import com.up_project.up_first_spring_project.model.request.ProductRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;

import java.util.List;

public interface ProductService {
    List<ProductProjection> getAllProduct(Pagination pagination);
    ProductProjection getProductById(long id);

    ProductProjection updateById(long id, ProductRequest productRequest);

    String deleteProductById(long id);

    ProductProjection addNewProduct(ProductRequest productRequest);
}
