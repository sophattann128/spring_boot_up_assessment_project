package com.up_project.up_first_spring_project.model.projection;

import jakarta.persistence.criteria.CriteriaBuilder;

import java.util.List;

public interface ShopProjection {

    Long getShopId();
    String getAddress();
    String getBranchName();

    List<Stock> getStocks();
    interface Stock{
        Long getStockId();
        Double getProductPrice();
        Product getProduct();
        Integer getProductAmount();

    }
    interface Product{
        String getProductName();
    }
}
