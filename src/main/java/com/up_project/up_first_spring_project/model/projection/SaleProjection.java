package com.up_project.up_first_spring_project.model.projection;

import java.time.LocalDateTime;

public interface SaleProjection {
    Long getSaleId ();
    LocalDateTime date();
    Shop getShop();
    User getUser();
    interface Shop{
        Long getShopId();
        String getAddress();
        String getBranchName();
    }
    interface User{
        Long getUserId();
        String getUsername();
    }

}
