package com.up_project.up_first_spring_project.model.projection;

public interface RoleProjection {
    int getRoleId();
     String getRole();
}
