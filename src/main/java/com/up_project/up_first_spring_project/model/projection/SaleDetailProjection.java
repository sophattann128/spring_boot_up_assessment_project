package com.up_project.up_first_spring_project.model.projection;

public interface SaleDetailProjection {
    Long getSaleDetailId();
    Long getAmount();
    Product getProduct();

    interface Sale{

    }

    interface Product{
        Category getCategory();
        Long getProductId();
        String getProductName();
        Long getProductPrice();

    }
    interface Category{
        Long getCategoryId();
        String getCategoryName();
        String getCategoryDescription();
    }
}
