package com.up_project.up_first_spring_project.model.projection;

public interface ProductProjection {
    Long getProductId();
    String getProductName();
    Double getProductPrice();
    Category getCategory();
    interface Category {
        Long getCategoryId();
        String getCategoryName();
        String getCategoryDescription();
    }
    interface Supplier{
        Long getSupplierId();
        String getCompanyName();
        String getPhoneNumber();
    }
}