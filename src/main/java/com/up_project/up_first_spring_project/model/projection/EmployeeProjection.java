package com.up_project.up_first_spring_project.model.projection;
import java.time.LocalDateTime;
public interface EmployeeProjection {
    Long getEmployeeId();
    String getFirstName();
    String getLastName();
    String getEmail();
    String getPhoneNumber();
    LocalDateTime getHiredDate();

    Job getJob();
    interface Job{
        Long getJobId();
        String getJobTitle();
        Double getSalary();
        LocalDateTime getUpdatedDate();
    }
    Location getLocation();
    interface Location{
        long getLocationId();
        String getProvince();
        String getCity();
        String getStreet();

    }
}
