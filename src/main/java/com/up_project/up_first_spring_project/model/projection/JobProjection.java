package com.up_project.up_first_spring_project.model.projection;

import java.time.LocalDateTime;

public interface JobProjection {
     int getJobId();
     String getJobTitle();
     double getSalary();
//     LocalDateTime getUpdateDate();
}
