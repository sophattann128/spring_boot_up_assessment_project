package com.up_project.up_first_spring_project.model.projection;

import org.springframework.beans.factory.annotation.Value;

public interface StockProjection {
    Long getStockId();
    Long getProductAmount();

    Product getProduct();
    interface Product{
        Long getProductId();
        String getProductName();
        Long getProductPrice();
        Category getCategory();

    }
    interface Category{
        Long getCategoryId();
        String getCategoryName();
        String getCategoryDescription();
    }

//    @Value("#{target.product.productId}")
//    Long getProductId();

}
