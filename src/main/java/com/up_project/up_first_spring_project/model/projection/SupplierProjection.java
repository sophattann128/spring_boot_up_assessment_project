package com.up_project.up_first_spring_project.model.projection;

import com.up_project.up_first_spring_project.model.entities.Location;

public interface SupplierProjection {
    Long getSupplierId();
    String getCompanyName();
    String getPhoneNumber();
    interface getLocation{
        Long getLocationId();
        String getProvince();
        String getCity();
        String getStreet();
    }
    Location getLocation();
}
