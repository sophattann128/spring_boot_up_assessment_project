package com.up_project.up_first_spring_project.model.projection;

import java.time.LocalDateTime;

public interface UserProjection {
    long getUserId();
     String getUsername();
     String getPassword();
     Role getRole();
     Employee getEmployee();

     interface Role{
          String getRole();
     }
     interface Employee{
         long getEmployeeId();
         String getFirstName();
         String getLastName();
         String getPhoneNumber();
         LocalDateTime getHiredDate();
     }

}
