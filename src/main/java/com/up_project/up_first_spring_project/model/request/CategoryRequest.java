package com.up_project.up_first_spring_project.model.request;

import lombok.Data;

@Data
public class CategoryRequest {
    private String categoryName;
    private String categoryDescription;
}
