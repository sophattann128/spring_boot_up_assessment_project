package com.up_project.up_first_spring_project.model.request;

import lombok.Data;

@Data
public class LocationRequest {
    private String province;
    private String city;
    private String street;
}
