package com.up_project.up_first_spring_project.model.request;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobRequest {
    private int jobId;
    private String jobTitle;
    private double salary;
}
