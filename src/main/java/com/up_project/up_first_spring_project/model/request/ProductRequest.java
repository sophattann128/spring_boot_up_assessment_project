package com.up_project.up_first_spring_project.model.request;

import lombok.Data;

@Data
public class ProductRequest {
    private String productName;
    private Double productPrice;
    private Long categoryId;


   private Long supplierId;
}
