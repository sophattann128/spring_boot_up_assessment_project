package com.up_project.up_first_spring_project.model.request;

import lombok.Data;

@Data
public class SupplierRequest {
    private String companyName;
    private String phoneNumber;
    private Long locationId;
}
