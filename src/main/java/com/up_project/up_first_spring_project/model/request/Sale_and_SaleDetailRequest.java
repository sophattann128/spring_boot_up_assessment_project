package com.up_project.up_first_spring_project.model.request;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Sale_and_SaleDetailRequest {
    private LocalDateTime date;
    private Long shopId;
    private long productId;
    private int amount;
}
