package com.up_project.up_first_spring_project.model.request;

import lombok.Data;

import java.time.LocalDateTime;
@Data
public class SaleRequest {
    private LocalDateTime date;
    private Long shopId;

}
