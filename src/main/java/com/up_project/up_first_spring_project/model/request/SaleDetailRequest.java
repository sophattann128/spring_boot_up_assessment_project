package com.up_project.up_first_spring_project.model.request;

import lombok.Data;

@Data
public class SaleDetailRequest {

    private int amount;
    private long productId;
    private long saleId;

}
