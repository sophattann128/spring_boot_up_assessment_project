package com.up_project.up_first_spring_project.model.request;

import com.up_project.up_first_spring_project.model.entities.Product;
import com.up_project.up_first_spring_project.model.entities.Shop;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import lombok.Data;

@Data
public class StockRequest{
        private double productPrice;
        private int productAmount;
        private long productId;
        private long shopId;
}
