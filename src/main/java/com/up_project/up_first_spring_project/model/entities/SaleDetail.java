package com.up_project.up_first_spring_project.model.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
public class SaleDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long saleDetailId;
    private int amount;


    @ManyToOne
    @JoinColumn(name="product_id", nullable=false)
    private Product product;
    @ManyToOne
    @JoinColumn(name="sale_id", nullable=false)
    private Sale sale;
}
