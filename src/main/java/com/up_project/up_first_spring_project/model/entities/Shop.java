package com.up_project.up_first_spring_project.model.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long shopId;
    private String address;
    private String branchName;

    @OneToMany(mappedBy="shop", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Stock> stocks;

}
