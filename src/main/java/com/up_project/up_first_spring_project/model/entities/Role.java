package com.up_project.up_first_spring_project.model.entities;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int roleId;
    private String role;

//    @OneToMany(mappedBy = "role")
//    @JsonManagedReference
//    private List<User> users;

//    @OneToMany(mappedBy="job")
//    private List<Employee> employees;
//    @ManyToOne
//    @JoinColumn(name="user_id", nullable=false)
//    private User user;
}
