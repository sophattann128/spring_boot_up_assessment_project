package com.up_project.up_first_spring_project.model.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long productId;
    private String productName;
    private double productPrice;
    @ManyToOne
    @JoinColumn(name="supplier_id", nullable=false)
    private Supplier supplier;
    @OneToOne
    @JoinColumn(name = "category_id")
    private Category category;
    @OneToMany(mappedBy = "product",  cascade = CascadeType.ALL)
    private List<Stock> stocks;

    @OneToMany(mappedBy="product", cascade = CascadeType.ALL)
    private List<SaleDetail> saleDetails;


}
