package com.up_project.up_first_spring_project.model.entities;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long employeeId;
    private String firstName;
    private String lastName;
    private String email;
    private String phoneNumber;


//    @OneToMany(mappedBy="employee")
//    private List<Job> jobs;
    private LocalDateTime hiredDate;

    @ManyToOne
    @JoinColumn(name="job_id", nullable=false)
    private Job job;
//    @OneToOne(cascade = CascadeType.ALL)
    @OneToOne
    @JoinColumn(name = "location_id")
//    @OnDelete(action = OnDeleteAction.CASCADE)
    private Location location;

    @OneToOne(fetch = FetchType.LAZY)
    private User user;

}
