package com.up_project.up_first_spring_project.model.entities;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Builder
public class Job {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int jobId;
    private String jobTitle;
    private double salary;
    private LocalDateTime updatedDate;

//    @ManyToOne
//    @JoinColumn(name="employee_id", nullable=false)
//    private Employee employee;

//    @OneToMany(mappedBy="job")
//    private List<Employee> employees;
//    @OneToMany(mappedBy="job")
//
//    private List<Employee> employee;
}
