package com.up_project.up_first_spring_project.model.entities;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Sale {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long saleId;
    private LocalDateTime date;



    @ManyToOne
    @JoinColumn(name="user_id", nullable=false)
    private User user;


    @ManyToOne
    @JoinColumn(name="shop_id", nullable=false)
    private Shop shop;

    @OneToMany(mappedBy = "sale", cascade = CascadeType.ALL)
    private List<SaleDetail> saleDetails;



}
