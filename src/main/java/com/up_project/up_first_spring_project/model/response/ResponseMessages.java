package com.up_project.up_first_spring_project.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessages<T>{
    T payload;
    LocalDateTime date;
    Boolean status;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Pagination pagination;


    public ResponseMessages(T payload, Boolean status) {
        this.payload = payload;
        this.date =  LocalDateTime.now();
        this.status = status;
    }
    public ResponseMessages(T payload, Boolean status, Pagination pagination) {
        this.payload = payload;
        this.date =  LocalDateTime.now();
        this.status = status;
        this.pagination = pagination;
    }
}
