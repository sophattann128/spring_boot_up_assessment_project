package com.up_project.up_first_spring_project.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ResponseMessagesDelete{
    String message;
    LocalDateTime date;
    Boolean status;

    public ResponseMessagesDelete(String message, Boolean status) {
        this.message = message;
        this.date = LocalDateTime.now();
        this.status = status;
    }
}
