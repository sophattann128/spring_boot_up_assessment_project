package com.up_project.up_first_spring_project.Repository;

import com.up_project.up_first_spring_project.model.entities.Stock;
import com.up_project.up_first_spring_project.model.projection.StockProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StockRepository extends JpaRepository<Stock, Long> {
    Page<StockProjection> findStockProjectionBy(Pageable pageable);
    StockProjection findStockProjectionByStockId(long id);
//    StockProjection saveStockProjection(Stock stock);
}
