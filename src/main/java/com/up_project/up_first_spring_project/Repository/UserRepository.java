package com.up_project.up_first_spring_project.Repository;
import com.up_project.up_first_spring_project.model.entities.User;
import com.up_project.up_first_spring_project.model.projection.RoleProjection;
import com.up_project.up_first_spring_project.model.projection.UserProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {
    UserProjection findByUserId(long id);
    Page<UserProjection> findUserProjectionBy(Pageable pageable);

    User findUserByUsername(String username);


}
