package com.up_project.up_first_spring_project.Repository;
import com.up_project.up_first_spring_project.model.entities.Job;
import com.up_project.up_first_spring_project.model.projection.JobProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JobRepository extends JpaRepository<Job, Long> {
    JobProjection findByJobId(long id);
    Page<JobProjection> findJobProjectionBy(Pageable pageable);

}
