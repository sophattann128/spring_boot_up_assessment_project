package com.up_project.up_first_spring_project.Repository;

import com.up_project.up_first_spring_project.model.entities.Product;
import com.up_project.up_first_spring_project.model.projection.ProductProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends JpaRepository<Product,Long> {
    Page<ProductProjection> findProductProjectionBy(Pageable pageable);
    ProductProjection findByProductId (long id);




}
