package com.up_project.up_first_spring_project.Repository;

import com.up_project.up_first_spring_project.model.entities.Role;
import com.up_project.up_first_spring_project.model.projection.JobProjection;
import com.up_project.up_first_spring_project.model.projection.RoleProjection;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Long> {
    RoleProjection findByRoleId(long id);
    List<RoleProjection> findRoleProjectionBy();
}
