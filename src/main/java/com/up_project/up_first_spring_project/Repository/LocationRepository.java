package com.up_project.up_first_spring_project.Repository;

import com.up_project.up_first_spring_project.model.entities.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Long> {
}
