package com.up_project.up_first_spring_project.Repository;

import com.up_project.up_first_spring_project.model.entities.Sale;
import com.up_project.up_first_spring_project.model.projection.SaleProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface SaleRepository extends JpaRepository<Sale,Long> {
    Page<SaleProjection> findSaleProjectionBy(Pageable pageable);
    SaleProjection findBySaleId ( long id);
    Sale findSaleBySaleId(long id);
    void deleteBySaleId(long id);

    @Query("SELECT s FROM Sale s WHERE s.user.userId = :userId")
    List<SaleProjection> getAllSaleFromCurrentUser(Long userId);
}
