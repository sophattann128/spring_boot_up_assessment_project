package com.up_project.up_first_spring_project.Repository;

import com.up_project.up_first_spring_project.model.entities.Shop;
import com.up_project.up_first_spring_project.model.projection.ShopProjection;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;

@Transactional
public interface ShopRepository extends JpaRepository<Shop, Long> {


    ShopProjection findByShopId(Long shopId);

}
