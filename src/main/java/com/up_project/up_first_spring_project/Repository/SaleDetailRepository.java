package com.up_project.up_first_spring_project.Repository;

import com.up_project.up_first_spring_project.model.entities.SaleDetail;
import com.up_project.up_first_spring_project.model.projection.SaleDetailProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SaleDetailRepository extends JpaRepository<SaleDetail, Long> {
    Page<SaleDetailProjection> findSaleDetailProjectionBy(Pageable pageable);
    SaleDetailProjection findSaleDetailBySaleDetailId(long id);
}
