package com.up_project.up_first_spring_project.Repository;

import com.up_project.up_first_spring_project.model.entities.Supplier;
import com.up_project.up_first_spring_project.model.projection.SupplierProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {
//    List<SupplierProjection> findAllBy();
}
