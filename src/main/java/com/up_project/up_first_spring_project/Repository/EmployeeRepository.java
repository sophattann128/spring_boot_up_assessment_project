package com.up_project.up_first_spring_project.Repository;
import com.up_project.up_first_spring_project.model.entities.Employee;
import com.up_project.up_first_spring_project.model.projection.EmployeeProjection;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
    EmployeeProjection findByEmployeeId(long id);
    Page<EmployeeProjection> findEmployeeProjectionBy(Pageable pageable);
}
