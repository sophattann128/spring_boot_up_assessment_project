package com.up_project.up_first_spring_project.controller;
import com.up_project.up_first_spring_project.model.entities.Employee;
import com.up_project.up_first_spring_project.model.entities.Job;
import com.up_project.up_first_spring_project.model.projection.EmployeeProjection;
import com.up_project.up_first_spring_project.model.request.EmployeeRequest;
import com.up_project.up_first_spring_project.model.request.JobRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.EmployeeService;
import com.up_project.up_first_spring_project.service.JobService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/employees")
@SecurityRequirement(name = "bearerAuth")
public class EmployeeController {
    private final EmployeeService employeeService;
    @GetMapping("/{id}")
    public ResponseEntity<?> getEmployeeById(@PathVariable long id){
        ResponseMessages<?> employee = new ResponseMessages<>(
                employeeService.findEmployeeById(id),
                true
        );
        return ResponseEntity.ok(employee);

    }

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllEmployee(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> employees = new ResponseMessages<>(
                employeeService.findAllEmployee(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(employees);

    }
    @PutMapping("/{id}")
    public ResponseEntity<?> updateEmployeeById(@PathVariable long id, @RequestBody EmployeeRequest employeeRequest){
        ResponseMessages<?> employee = new ResponseMessages<>(
                employeeService.updateEmployeeById(id, employeeRequest),
                true
        );
        return ResponseEntity.ok(employee);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteEmployeeById(@PathVariable long id){
        ResponseMessages<?> employee = new ResponseMessages<>(
                "Deleted Successfully",
                true
        );
       employeeService.deleteEmployeeById(id);
       return ResponseEntity.ok(employee);
    }
    @PostMapping
    public ResponseEntity<?> insertEmployee(@RequestBody EmployeeRequest employeeRequest){
        ResponseMessages<?> employee = new ResponseMessages<>(
                employeeService.addEmployee(employeeRequest),
                true
        );
        return ResponseEntity.ok(employee);
    }
}
