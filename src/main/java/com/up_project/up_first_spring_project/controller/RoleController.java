package com.up_project.up_first_spring_project.controller;
import com.up_project.up_first_spring_project.model.entities.Role;
import com.up_project.up_first_spring_project.model.entities.User;
import com.up_project.up_first_spring_project.model.projection.RoleProjection;
import com.up_project.up_first_spring_project.model.request.RoleRequest;
import com.up_project.up_first_spring_project.model.request.UserRequest;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.RoleService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/roles")
@SecurityRequirement(name = "bearerAuth")

public class RoleController {
    private final RoleService roleService;
    @GetMapping("{id}")
    public ResponseEntity<?> getRoleById(@PathVariable long id){
        ResponseMessages<?> role = new ResponseMessages<>(
                roleService.findRoleById(id),
                true
        );
        return ResponseEntity.ok(role);

    }
    @GetMapping
    public ResponseEntity<?> getAllRole(){
        ResponseMessages<?> roles = new ResponseMessages<>(
                roleService.findAllRole(),
                true
        );
        return ResponseEntity.ok(roles);

    }
    @PutMapping("/{id}")
    public ResponseEntity<?> updateRoleById(@PathVariable long id, @RequestBody RoleRequest roleRequest){
        ResponseMessages<?> role = new ResponseMessages<>(
                roleService.updateRoleById(id, roleRequest),
                true
        );
        return ResponseEntity.ok(role);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteRoleById(@PathVariable long id){
        roleService.deleteRoleById(id);
        ResponseMessages<?> role = new ResponseMessages<>(
                "Deleted successfully",
                true
        );
        return ResponseEntity.ok(role);

    }
    @PostMapping("/role")
    public ResponseEntity<?> insertRole(@RequestBody RoleRequest roleRequest){
        ResponseMessages<?> role = new ResponseMessages<>(
                roleService.addRole(roleRequest),
                true
        );
        return ResponseEntity.ok(role);

    }

}
