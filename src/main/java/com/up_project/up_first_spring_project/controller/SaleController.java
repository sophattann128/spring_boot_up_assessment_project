package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.model.projection.SaleProjection;
import com.up_project.up_first_spring_project.model.request.SaleRequest;
import com.up_project.up_first_spring_project.model.request.Sale_and_SaleDetailRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.SaleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/sale")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class SaleController {
    private final SaleService saleService;

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllSale(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> sales = new ResponseMessages<>(
                saleService.getAllSale(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(sales);

    }

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping("/currentUser")
    public ResponseEntity<?> getAllSaleFromCurrentUser(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> sales = new ResponseMessages<>(
                saleService.getAllSaleFromCurrentUser(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(sales);

    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getSaleById(@PathVariable  long  id){
        ResponseMessages<?> sale = new ResponseMessages<>(
                saleService.getSaleById(id),
                true
        );
        return ResponseEntity.ok(sale);

    }

    @PostMapping
    public ResponseEntity<?> addNewSale(@RequestBody Sale_and_SaleDetailRequest saleRequest){
        ResponseMessages<?> sale = new ResponseMessages<>(
                saleService.addNewSale(saleRequest),
                true
        );
        return ResponseEntity.ok(sale);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateSale(@PathVariable long id, @RequestBody SaleRequest saleRequest){
        ResponseMessages<?> sale = new ResponseMessages<>(
                saleService.updateSale(id, saleRequest),
                true
        );
        return ResponseEntity.ok(sale);

    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSale(@PathVariable long id){

        saleService.deleteSale(id);
        ResponseMessages<?> sale = new ResponseMessages<>(
                "Deleted successfully",
                true
        );
        return ResponseEntity.ok(sale);
    }


}
