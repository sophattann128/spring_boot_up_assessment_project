package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.model.entities.Location;
import com.up_project.up_first_spring_project.model.request.LocationRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.LocationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/location")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class LocationController {
    private final LocationService locationService;

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllLocations(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> locations = new ResponseMessages<>(
                locationService.getAllLocations(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(locations);

    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getAllById(@PathVariable long id){
        ResponseMessages<?> locations = new ResponseMessages<>(
                locationService.getLocationsById(id),
                true
        );
        return ResponseEntity.ok(locations);

    }
    @PostMapping
    public ResponseEntity<?> addNewLocation(@RequestBody LocationRequest locationRequest){
        ResponseMessages<?> locations = new ResponseMessages<>(
                locationService.addNewLocation(locationRequest),
                true
        );
        return ResponseEntity.ok(locations);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateLocation(@PathVariable long id, @RequestBody LocationRequest locationRequest){
        ResponseMessages<?> locations = new ResponseMessages<>(
                locationService.updateLocation(id, locationRequest),
                true
        );
        return ResponseEntity.ok(locations);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteLocation(@PathVariable long id){
        ResponseMessages<?> locations = new ResponseMessages<>(
                "Deleted successfully",
                true
        );
        return ResponseEntity.ok(locations);

    }
}
