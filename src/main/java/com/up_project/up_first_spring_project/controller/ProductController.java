package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.model.projection.ProductProjection;
import com.up_project.up_first_spring_project.model.request.ProductRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("api/v1/products")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllProduct(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> products = new ResponseMessages<>(
                productService.getAllProduct(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(products);

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getProductById(@PathVariable long id) {
        ResponseMessages<?> product = new ResponseMessages<>(
                productService.getProductById(id),
                true
        );
        return ResponseEntity.ok(product);

    }

    @PostMapping
    public ResponseEntity<?> addNewProduct(@RequestBody ProductRequest productRequest){
        ResponseMessages<?> product = new ResponseMessages<>(
                productService.addNewProduct(productRequest),
                true
        );
        return ResponseEntity.ok(product);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateById(@PathVariable long id , @RequestBody ProductRequest productRequest){
        ResponseMessages<?> product = new ResponseMessages<>(
                productService.updateById(id, productRequest),
                true
        );
        return ResponseEntity.ok(product);
    }
    @DeleteMapping({"/{id}"})
    public ResponseEntity<?> deleteProductById(@PathVariable long id){
        productService.deleteProductById(id);
        ResponseMessages<?> product = new ResponseMessages<>(
                "Deleted successfully",
                true
        );
        return ResponseEntity.ok(product);

    }

}
