package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.model.entities.SaleDetail;
import com.up_project.up_first_spring_project.model.projection.SaleDetailProjection;
import com.up_project.up_first_spring_project.model.request.SaleDetailRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.SaleDetailService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/saleDetails")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class SaleDetailController {
    private final SaleDetailService saleDetailService;

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllSaleDetail(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> saleDetails = new ResponseMessages<>(
                saleDetailService.getAllSaleDetail(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(saleDetails);

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getSaleDetailById(@PathVariable long id){
        ResponseMessages<?> saleDetail = new ResponseMessages<>(
                saleDetailService.getSaleDetailById(id),
                true
        );
        return ResponseEntity.ok(saleDetail);
    }

    @PostMapping
    public ResponseEntity<?> addSaleDetail(@RequestBody SaleDetailRequest saleDetailRequest){
        ResponseMessages<?> saleDetail = new ResponseMessages<>(
                saleDetailService.addSaleDetail(saleDetailRequest),
                true
        );
        return ResponseEntity.ok(saleDetail);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateSaleDetail(@PathVariable long id, @RequestBody SaleDetailRequest saleDetailRequest){
        ResponseMessages<?> saleDetail = new ResponseMessages<>(
                saleDetailService.updateSaleDetail(id, saleDetailRequest),
                true
        );
        return ResponseEntity.ok(saleDetail);
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSaleDetail(@PathVariable long id){
        ResponseMessages<?> message = new ResponseMessages<>(
                "This saleDetail is deleted successfully",
                true
        );
        saleDetailService.deleteSaleDetail(id);
        return ResponseEntity.ok(message);
    }
}
