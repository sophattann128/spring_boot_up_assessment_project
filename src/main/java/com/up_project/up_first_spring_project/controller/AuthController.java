package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.JWT.AuthenticationRequest;
import com.up_project.up_first_spring_project.JWT.AuthenticationResponse;
import com.up_project.up_first_spring_project.JWT.JwtTokenUtils;
import com.up_project.up_first_spring_project.model.entities.User;
import com.up_project.up_first_spring_project.model.request.UserRequest;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins= {"*"}, maxAge = 4800, allowCredentials = "false" )
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
@SecurityRequirement(name = "bearerAuth")
public class AuthController {

    private final UserService myUserService;

    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtils jwtTokenUtils;


    //Register
    @PostMapping("/register")
    @Operation(summary = "register")
    public ResponseEntity<?> register(@RequestBody UserRequest myUserRequest){
        User myUserDTO = myUserService.addUser(myUserRequest);
        if(myUserDTO != null){
            ResponseMessages<?> responseMessages = new ResponseMessages<>(
                    myUserDTO,
                    true
            );
            return ResponseEntity.ok().body(responseMessages);
        }
        return ResponseEntity.badRequest().body(
               new ResponseMessages<>(
                "This user is already exist!!!",
                false
        ));

    }
    @PostMapping("/login")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        AuthenticationResponse authenticationResponse = new AuthenticationResponse();
        final UserDetails userDetails = myUserService
                .loadUserByUsername(authenticationRequest.getUsername());
        final String token = jwtTokenUtils.generateToken(userDetails);

        String loginUsername = authenticationRequest.getUsername();
        String equalUsername = myUserService.getUserName(loginUsername);

        if(loginUsername.equals(equalUsername)){
            long userId = myUserService.getUserId(equalUsername);
            authenticationResponse.setUserId(userId);
        }
        authenticationResponse.setToken(token);
        authenticationResponse.setUsername(authenticationRequest.getUsername());
        ResponseMessages<?> responseMessages = new ResponseMessages<>(
                authenticationResponse,
                true
        );

        return ResponseEntity.ok().body(responseMessages);
    }



    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }


}
