package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.model.entities.Shop;
import com.up_project.up_first_spring_project.model.request.ShopRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.ShopService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.parameters.P;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/shop")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class ShopController {
    private final ShopService shopService;
    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllShops(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> shops = new ResponseMessages<>(
                shopService.getAllShops(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(shops);

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getShopById(@PathVariable long id){
        ResponseMessages<?> shop = new ResponseMessages<>(
                shopService.getAllShopsById(id),
                true
        );
        return ResponseEntity.ok(shop);

    }

    @PostMapping
    public ResponseEntity<?> addNewShop(@RequestBody ShopRequest shopRequest){
        ResponseMessages<?> shop = new ResponseMessages<>(
                shopService.addNewShop(shopRequest),
                true
        );
        return ResponseEntity.ok(shop);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateShop(@PathVariable long id, @RequestBody ShopRequest shopRequest){
        ResponseMessages<?> shop = new ResponseMessages<>(
                shopService.updateShop(id, shopRequest),
                true
        );
        return ResponseEntity.ok(shop);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteShop(@PathVariable long id){
        shopService.deleteShop(id);
        ResponseMessages<?> shop = new ResponseMessages<>(
                "Deleted successfully",
                true
        );
        return ResponseEntity.ok(shop);

    }
}
