package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Category;
import com.up_project.up_first_spring_project.model.entities.Stock;
import com.up_project.up_first_spring_project.model.projection.StockProjection;
import com.up_project.up_first_spring_project.model.request.StockRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.StockService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.webjars.NotFoundException;

import java.util.List;

@RestController
@RequestMapping("/api/v1/stocks")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class StockController {
    private final StockService stockService;

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllStock(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> stocks = new ResponseMessages<>(
                stockService.getAllStock(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(stocks);

    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getStockById(@PathVariable long id) {
        if (stockService.getStockById(id) == null) {
            throw new NotFoundExceptionClass("This stock id doesn't exist!", "Stock");
        }
        ResponseMessages<?> stock = new ResponseMessages<>(
                stockService.getStockById(id),
                true
        );
        return ResponseEntity.ok(stock);
    }

    @PostMapping
    public ResponseEntity<?> addNewStock(@RequestBody StockRequest stockRequest){
        ResponseMessages<?> stock = new ResponseMessages<>(
                stockService.addNewStock(stockRequest),
                true
        );
        return ResponseEntity.ok(stock);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateStock(@PathVariable long id, @RequestBody StockRequest stockRequest){
        ResponseMessages<?> stock = new ResponseMessages<>(
                stockService.updateStock(id,stockRequest),
                true
        );
        return ResponseEntity.ok(stock);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteStock(@PathVariable long id){
        ResponseMessages<?> message = new ResponseMessages<>(
                "This stock is deleted successfully",
                true
        );
        stockService.deleteStockById(id);
        return ResponseEntity.ok(message);
    }


}
