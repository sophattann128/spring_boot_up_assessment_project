package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.exception.NotFoundExceptionClass;
import com.up_project.up_first_spring_project.model.entities.Category;
import com.up_project.up_first_spring_project.model.request.CategoryRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.CategoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/categories")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;
    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })

    @GetMapping
    public ResponseEntity<?> getAllCategories(@Parameter(hidden = true) Pagination pagination) {
        List<Category> categoryList = categoryService.getAllCategories(pagination);
        ResponseMessages<?> categories = new ResponseMessages<>(
                categoryList,
                true,
                pagination
        );

        return ResponseEntity.ok(categories);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getCategoryById(@PathVariable Long id) {
        Category categoryById = categoryService.getCategoryById(id);
        if (categoryById == null) {
            throw new NotFoundExceptionClass("This category id doesn't exist!", "category");
        }
        ResponseMessages<?> category = new ResponseMessages<>(
                categoryById,
                true
        );
        return ResponseEntity.ok(category);
    }

    @PostMapping
    public ResponseEntity<?> addCategory(@RequestBody CategoryRequest categoryRequest) {
        ResponseMessages<?> category = new ResponseMessages<>(
                categoryService.addCategory(categoryRequest),
                true
        );
        return ResponseEntity.ok(category);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody CategoryRequest categoryRequest) {
        ResponseMessages<?> category = new ResponseMessages<>(
                categoryService.update(id, categoryRequest),
                true
        );
        return ResponseEntity.ok(category);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteById(@PathVariable Long id) {
        Category foundCategory = categoryService.getCategoryById(id);
        if (foundCategory == null) {
            throw new NotFoundExceptionClass("This category id doesn't exist!", "category");
        }
        ResponseMessages<?> message = new ResponseMessages<>(
                "This category is deleted successfully",
                true
        );
        categoryService.deleteById(id);
        return ResponseEntity.ok(message);
    }

}
