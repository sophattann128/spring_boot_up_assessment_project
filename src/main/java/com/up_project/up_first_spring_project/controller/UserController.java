package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.model.entities.User;
import com.up_project.up_first_spring_project.model.projection.UserProjection;
import com.up_project.up_first_spring_project.model.request.UserRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/users")
@SecurityRequirement(name = "bearerAuth")

public class UserController {
    private final UserService userService;

    @GetMapping("/{id}")
    public UserProjection getUserById(@PathVariable long id) {
        return userService.findUserById(id);

    }

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllUser(@Parameter(hidden = true) Pagination pagination) {
        ResponseMessages<?> users = new ResponseMessages<>(
                userService.findAllUser(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(users);

    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUserById(@PathVariable long id, @RequestBody UserRequest userRequest) {
        ResponseMessages<?> user = new ResponseMessages<>(
                userService.updateUserById(id, userRequest),
                true
        );
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUserById(@PathVariable long id) {
        userService.deleteUserById(id);
        ResponseMessages<?> user = new ResponseMessages<>(
                "Deleted successfully",
                true
        );
        return ResponseEntity.ok(user);
    }

//    @PostMapping
//    public ResponseEntity<?> insertUser(@RequestBody UserRequest userRequest) {
//        ResponseMessages<?> user = new ResponseMessages<>(
//                userService.addUser(userRequest),
//                true
//        );
//        return ResponseEntity.ok(user);
//    }
}
