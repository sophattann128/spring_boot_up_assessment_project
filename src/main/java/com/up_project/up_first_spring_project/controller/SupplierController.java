package com.up_project.up_first_spring_project.controller;

import com.up_project.up_first_spring_project.model.entities.Supplier;
import com.up_project.up_first_spring_project.model.request.SupplierRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.SupplierService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/suppliers")
@SecurityRequirement(name = "bearerAuth")
@RequiredArgsConstructor
public class SupplierController {
    private final SupplierService supplierService;


    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllSuppliers(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> suppliers = new ResponseMessages<>(
                supplierService.getAllSuppliers(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(suppliers);

    }


    @GetMapping("/{id}")
    public ResponseEntity<?> getAllSupplierById(@PathVariable long id){
        ResponseMessages<?> supplier = new ResponseMessages<>(
                supplierService.getSupplierById(id),
                true
        );
       return ResponseEntity.ok(supplier);
    }

    @PostMapping
    public ResponseEntity<?> addNewSupplier(@RequestBody SupplierRequest supplierRequest){
        ResponseMessages<?> supplier = new ResponseMessages<>(
                supplierService.addNewSupplier(supplierRequest),
                true
        );
        return ResponseEntity.ok(supplier);
    }

    @PutMapping("/{id}")
public ResponseEntity<?> updateSupplier(@PathVariable long id, @RequestBody SupplierRequest supplierRequest){
        ResponseMessages<?> supplier = new ResponseMessages<>(
                supplierService.updateSupplier(id, supplierRequest),
                true
        );
        return ResponseEntity.ok(supplier);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteSupplier(@PathVariable long id){
        supplierService.deleteSupplier(id);
        ResponseMessages<?> supplier = new ResponseMessages<>(
                "The supplier id " + id + " has successfully deleted!",
                true
        );
        return ResponseEntity.ok(supplier);
    }

}
