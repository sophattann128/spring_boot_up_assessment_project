package com.up_project.up_first_spring_project.controller;
import com.up_project.up_first_spring_project.model.entities.Job;
import com.up_project.up_first_spring_project.model.projection.JobProjection;
import com.up_project.up_first_spring_project.model.request.JobRequest;
import com.up_project.up_first_spring_project.model.response.Pagination;
import com.up_project.up_first_spring_project.model.response.ResponseMessages;
import com.up_project.up_first_spring_project.service.JobService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/jobs")
@SecurityRequirement(name = "bearerAuth")
public class JobController {
    private final JobService jobService;
    @GetMapping("/{id}")
    public ResponseEntity<?> getJobById(@PathVariable long id){
        ResponseMessages<?> job = new ResponseMessages<>(
                jobService.findJobById(id),
                true
        );
        return ResponseEntity.ok(job);

    }

    @Operation(summary = "Find Data")
    @Parameters({
            @Parameter(in = ParameterIn.QUERY
                    , description = "Page you want to retrieve (1..N)"
                    , name = "page"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "1"))),
            @Parameter(in = ParameterIn.QUERY
                    , description = "Number of records per page."
                    , name = "size"
                    , content = @Content(schema = @Schema(type = "integer", defaultValue = "20")))
    })
    @GetMapping
    public ResponseEntity<?> getAllJob(@Parameter(hidden = true) Pagination pagination){
        ResponseMessages<?> jobs = new ResponseMessages<>(
                jobService.findAllJob(pagination),
                true,
                pagination
        );
        return ResponseEntity.ok(jobs);

    }
    @PutMapping({"/{id}"})
    public ResponseEntity<?> updateJobById(@PathVariable long id, @RequestBody JobRequest jobRequest){
        ResponseMessages<?> job = new ResponseMessages<>(
                jobService.updateJobById(id,  jobRequest),
                true
        );
        return ResponseEntity.ok(job);
    }
    @DeleteMapping({"/{id}"})
    public ResponseEntity<?> deleteJobById(@PathVariable long id){
        ResponseMessages<?> message = new ResponseMessages<>(
                "Deleted Successfully",
                true
        );

        jobService.deleteJobById(id);
        return ResponseEntity.ok(message);
    }
    @PostMapping
    public ResponseEntity<?> insertJob(@RequestBody JobRequest jobRequest){
        ResponseMessages<?> message = new ResponseMessages<>(
                jobService.addJob(jobRequest),
                true
        );

        return  ResponseEntity.ok(message);
    }
}
