package com.up_project.up_first_spring_project.exception;

public class ForbiddenException extends RuntimeException {
    private String title;
    public ForbiddenException(String message, String title) {
        super(message);
    }

    public String getTitle() {
        return title;
    }
}
